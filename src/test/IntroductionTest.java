import java.io.*;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;

public class IntroductionTest {

  private String[] getMainOutput() {
    ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    System.setOut(new PrintStream(outputStreamCaptor));
    Introduction.main(null);
    return outputStreamCaptor.toString().split("\\r?\\n");
  }

  @Test
  public void helloWorldMainTest() {
    String[] mainOut = getMainOutput();
    Assert.assertEquals(mainOut[0], "Hello world!");
  }

  @Test
  public void coinFlipTest() {
    int loops = 10000;
    double heads = 0;
    String side;
    for (int i = 0; i < loops; i++) {
      side = Introduction.coinFlip();
      if (side.equals("heads")) {
        heads++;
        continue;
      }
      Assert.assertTrue(side.equals("tails"));
    }
    Assert.assertTrue(heads / loops > 0.5 && heads / loops < 0.6);
  }

  @Test
  public void coinFlipMainTest() {
    String[] mainOut = getMainOutput();
    int heads = Integer.parseInt(mainOut[1]);
    Assert.assertTrue(heads > 10 && heads < 90);
  }

  @Test
  public void checkSumTest() {
    Assert.assertEquals(Introduction.checkSum(new int[]{5, 7, 8, 9, 10, 15, 16}), 0);
    Assert.assertEquals(Introduction.checkSum(new int[]{3, 5, 8, 9, 10, 15, 16}), 1);
    Assert.assertEquals(Introduction.checkSum(new int[]{3, 4, 6, 9, 10, 14, 15}), 2);
    Assert.assertEquals(Introduction.checkSum(new int[]{6, 7, 8, 9, 10, 15, 16}), -1);
    Assert.assertEquals(Introduction.checkSum(new int[]{-20, 0, 5, 10, 30, 40, 50}), 0);
    Assert.assertEquals(Introduction.checkSum(new int[]{-1, 0, 20, 30, 40, 50, 40}), 1);
    Assert.assertEquals(Introduction.checkSum(new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}), 9);
    Assert.assertEquals(Introduction.checkSum(new int[]{0, 2, 4, 16, 18, 19}), 1);
    Assert.assertEquals(Introduction.checkSum(new int[]{2, 4, 16, 18, 19}), 0);
    Assert.assertEquals(Introduction.checkSum(new int[]{0, 1, 19}), 1);
    Assert.assertEquals(Introduction.checkSum(new int[]{0}), -1);
    Assert.assertEquals(Introduction.checkSum(new int[]{}), -1);
  }

}
